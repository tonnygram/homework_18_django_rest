from django.db import models


class File(models.Model):
    name = models.CharField(max_length=50)
    file_to_be_uploaded = models.FileField(upload_to='upload')

    class Meta:
        verbose_name = "File"
        verbose_name_plural = "Files"
