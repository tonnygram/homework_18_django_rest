from django.contrib import admin
from .models import File


# admin:admin


class FileAdmin(admin.ModelAdmin):
    list_display = ('name', 'file_to_be_uploaded')
    list_display_links = ('name',)
    search_fields = ('name',)


admin.site.register(File, FileAdmin)
